﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp115
{
    class Program
    {
        static void Main(string[] args)
        {
            int a=
            Steps(8);
            Console.WriteLine(a);
        }
        public static int Steps(int number)
        {
            // Error Case
            if (number < 1) throw new ArgumentException("number must be greater than 0");

            // General Case
            int stepCount = 0;

            while (number != 1)
            {
                number = (number % 2 == 0) ? number / 2 : 3 * number + 1;
                ++stepCount;
            }

            return stepCount;
        }
    }
}
